# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
import configparser


DEFAULT_CFG = {"debug": False, "port": 8986, "address": "localhost"}


DEFAULT_PATHS = [
    "./fmcfg.ini",
    "~/.config/fdroid-monitor/fmcfg.ini",
    "/etc/fdroid-monitor/fmcfg.ini",
]


cfg = None


def _init(args):
    cfg = dict(DEFAULT_CFG)
    paths = list(DEFAULT_PATHS)
    if args.config:
        paths = args.config.split(":")
    for config_path in paths:
        config_path = os.path.abspath(os.path.expanduser(config_path))
        if os.path.exists(config_path):
            print(f"loading config '{config_path}'")
            with open(config_path, "r", encoding="utf-8") as f:
                raw = f.read()
                if "[DEFAULT]\n" not in raw:
                    raw = "[DEFAULT]\n" + raw
                config = configparser.ConfigParser()
                config.read_string(raw)
                for key, value in config["DEFAULT"].items():
                    if key not in DEFAULT_CFG.keys():
                        # TODO: raise config exception
                        pass
                    cfg[key] = value
                if len(config.sections()) > 0:
                    # TODO: raise config exception
                    pass
                break
    for key in DEFAULT_CFG.keys():
        if getattr(args, key, None):
            cfg[key] = getattr(args, key)
    # if cfg['debug']:
    #     for key, value in cfg.items():
    #         print(f'    {key}: {value}')
    return cfg


def load(args=None):
    global cfg
    if not cfg:
        cfg = _init(args)
    return cfg
